var utils = require('./util');

var PROPERTY = 1;
var CALL     = 2;
var WAIT     = 3;
var COLLECT  = 4;
var END      = 5;

exports.try_server = function (true_window, callback, console_obj, env_obj) {
    var env = env_obj || process.env;
    callback = callback || function () {};
    console_obj = console_obj || console;
    if (!env.LOVELOCK_SERVER) { callback(null); return false; }

    var password = env.LOVELOCK_PASSWORD || false;
    var host     = env.LOVELOCK_HOST;
    var port     = env.LOVELOCK_PORT;
    exports.server(true_window, host, port, password, function (server) {
        console_obj.log(utils.SPECIAL_CODE);
        callback(server);
    });

    return true;
};

exports.server = function (true_window, host, port, password, callback) {
    var http = require('http');
    var opts, server, err_str;

    var handler = function (request, response) {
        var event_data = [];

        // special kill signal:
        if (request.path === '/die') {
            response.writeHead(200);
            response.end("goodbye");
            server.close(function () {
                process.exit(0);
            });
            return;
        }

        var do_response = function (err, payload) {
            var result = {
                //uuid: uuid,
                payload: payload,
            };

            if (err) {
                var msg = "Undefined property '" + err + "', at chain: "
                        + err_str;
                //utils.event_data_to_string(parsed);
                if (opts.fatal_value_error) {
                    throw new Error(msg);
                } else {
                    console.error(msg);
                }
            } else {

                try {
                    var serialized = JSON.stringify(result)
                } catch (e) {
                    var msg = "Could not serialize payload for: " +
                            utils.event_data_to_string(parsed);
                    if (opts.fatal_value_error) {
                        throw new Error(msg);
                    } else {
                        console.error(msg);
                        delete result.payload; // ensure payload is "undefined"
                    }
                }
            }

            serialized = JSON.stringify(result);
            response.writeHead(200, {'Content-Type': 'application/json'});
            response.end(serialized);
        };

        request.on('data', function (chunk) {
            event_data.push(chunk + '');
        });

        request.on('end', function () {
            var parsed = JSON.parse(event_data.join(''));
            opts = parsed.options || {};
            err_str = parsed.err_str || '';
            //var uuid = parsed.uuid;

            if (password !== false) {
                if (parsed.password.trim() !== password.trim()) {
                    throw new Error("Password error");
                }
            }

            do_event(true_window, parsed, do_response);
        });
    };

    server = http.createServer(handler);

    server.listen(port, function () {
        callback(server);
    });

};

var do_event = function (true_window, event_data, callback) {
    // Actually doing the event starts here
    var retval = true_window;
    var action_stack = event_data.actions.slice();
    var result = null;

    var done = function () {
        if (result !== null) {
            // "collect" case
            callback(null, result);
        } else {
            // normal case
            callback(null, retval);
        }
    };



    var do_next = function () {
        if (action_stack.length < 1) {
            // nothing left!
            done();
            return;
        }

        var next_action = action_stack.shift();

        var valid_property = typeof retval === 'object'
            && typeof retval[next_action.name] !== "undefined";

        switch (next_action.type) {

            case PROPERTY:
                if (!valid_property) { callback(next_action.name); return; }
                retval = retval[next_action.name];
                break;

            case CALL:
                if (!valid_property) { callback(next_action.name); return; }
                var fnc = retval[next_action.name];
                retval = fnc.apply(retval, next_action.args);
                break;

            case WAIT:
                // pause X seconds until next iteration
                setTimeout(do_next, next_action.timeout);
                return; // prevent the do_next
                break;

            case COLLECT:
                // Collect current return value
                if (!result) { result = {}; }
                result[next_action.value_name] = retval;
                break;

            case END:
                // Reset back to true_window
                retval = true_window;
                break;

            default:
                throw new Error("Unknown action type: " + next_action.type);
        }

        do_next();
    };

    do_next();
};

var new_magic_obj = function (sender, baked) {
    var baked = baked || null;

    var chain_str = [];

    var action_queue = null;
    var obj = null;
    var last_item = null;

    var start_chain = function () {
        action_queue = [];
        last_item = null;
    };

    var end_chain = function (callback) {
        var actions = (baked || []).concat(action_queue);
        action_queue = null;
        sender({actions: actions, action: "action",
                err_str: chain_str.join('')}, callback);
    };

    var call_trap = function () {
        var args = Array.prototype.slice.call(arguments);
        switch (last_item.name) {
            case '_wait':
                last_item.type = WAIT;
                last_item.timeout = args[0];
                break;
            case '_collect':
                last_item.type = COLLECT;
                last_item.value_name = args[0];
                break;
            case '_end':
                last_item.type = END;
                if (baked) {
                    action_queue = action_queue.concat(baked);
                }
                break;
            default:
                last_item.type = CALL;
                last_item.args = args;
                break;
        }

        // for error reporting, also early error for invalid data
        chain_str.push(JSON.stringify(args).replace('[', '(').replace(']', ')'));
        return obj;
    };

    var construct = function () {
        var result = new_magic_obj(sender, action_queue);
        action_queue = null;
        return result;
    };

    var handler = {
        get: function (target, name) {
            if (!action_queue) {
                start_chain();
            }

            if (name === '_get') {
                return end_chain;
            }

            last_item = {
                type: PROPERTY,
                name: name,
            };

            chain_str.push('.' + name); // for error reporting

            action_queue.push(last_item);
            return obj;
        },
        //apply: call_trap,
    };

    obj = new Proxy.createFunction(handler, call_trap, construct);
    return obj;
};



var post_request = function (opts, data, cb, path) {
    //data.uuid = next_uuid();
    data.opts = opts._remote_opts;
    path = path || '/';
    if (opts.password) {
        data.password = opts.password;
    }

    //airborne[data.uuid] = callback;
    //client.write(JSON.stringify(data))
    var data_s = JSON.stringify(data);
    utils.http_request(opts.host, path, opts.port, 'POST', data_s, function (event_data_str) {
        var return_data = JSON.parse(event_data_str);
        //delete airborne[return_data.uuid]; // clear
        if (!("payload" in return_data)) {
            if (opts.fatal_value_error) {
                throw new Error("Value Error");
            } else {
                console.error("Value Error, not calling callback");
            }
        } else {
            cb(return_data.payload);
        }
    });
};



/*
Connect to an Lovelock server to begin testing.

Returns a faux object immediately usable, able to connect to server at
opts.port and opts.host.
*/

exports.connect_remote = function (opts) {
    opts.remote = true;
    return _prepare_window(opts);
};

/*
Start connect to an Lovelock server to begin testing.

Execute your electron application specified by opts.path and
opts.args, and call the callback with a bridge back.
*/
exports.launch = function (opts, callback) {
    var kill = require('tree-kill');
    opts.remote = false;
    var fake_window = _prepare_window(opts, callback);

    if (opts.password !== false) {
        opts.password = opts.password || Math.random().toString().slice(2);
        opts.env.LOVELOCK_PASSWORD = opts.password;
    }
    utils.check_opts(opts, ['path', 'args']);
    utils.run(opts, function (child) {
        var teardown = function (callback) {
            /*
            var cb = function () {
                child.kill('SIGTERM'); };
            post_request(opts, {}, cb, '/die');
            */
            kill(child.pid, 'SIGTERM', callback);
        };
        callback(fake_window, teardown);
    });
};

var _prepare_window = function (opts) {
    opts.port = opts.port || 9797;
    opts.host = opts.host || 'localhost';
    opts._remote_opts = {
        fatal_value_error: opts.fatal_value_error || false,
    };

    opts.env = opts.env || {};

    opts.env.LOVELOCK_HOST = opts.host;
    opts.env.LOVELOCK_PORT = opts.port + '';
    opts.env.LOVELOCK_SERVER = 'true';

    // Wrap opts to make a data sender
    var sender = function (data, cb) {
        return post_request(opts, data, cb);};

    // Create fake window object
    var fake_window = new_magic_obj(sender);
    return fake_window;
};


/*
Wrapper around exports.launch that uses `which' to automatically detect the
location of the electron binary.

Optional additional arg: "electron_project_path", should be a path to the root
of your electron project. (Default: ".")
*/
exports.launch_electron = function (opts, callback) {
    var child_process = require('child_process');
    child_process.exec("which electron", function (error, stdout, stderr) {
        var path = (stdout || '').trim();
        if (error !== null || path.length < 1) {
            throw new Error("Could not find electron binary");
        } else {
            var project_path = (opts.electron_project_path || '.');
            opts.path = path;
            opts.args = [project_path].concat(opts.args || []);
            exports.launch(opts, callback);
        }
    });
};

exports._new_magic_obj = new_magic_obj;
exports._do_event = do_event;

