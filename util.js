
exports.SPECIAL_CODE = "serverready:ho6y7qiWpoykUKEyJY3voJa7yYaiQoqTu6YU";

exports.extend = function (a, b, c) {
    var args = Array.prototype.slice.call(arguments);
    var a = args.shift();
    for (var i in args) {
        for (var k in args[i]) {
            a[k] = args[i][k];
        }
    }
    return a;
};

exports.http_request = function (host, path, port, method, data, callback) {
    var http = require('http');

    //The url we want is `www.nodejitsu.com:1337/`
    var options = { host: host, path: path, port: port, method: method };

    var req = http.request(options, function(response) {
        var chunks = [];
        response.on('data', function (chunk) {
            chunks.push(chunk + '');
        });

        response.on('end', function () {
            callback(chunks.join(''));
        });
    });

    //This is the data we are posting, it needs to be a string or a buffer
    if (data !== null) {
        req.write(data);
    }
    req.end();
};

exports.check_opts = function (opts, fields) {
    for (var i in fields) {
        var field = fields[i];
        if (!opts[field]) {
            throw new Error("ArgumentError: " + field + " is required");
        }
    }
};

exports.event_data_to_string = function (event_data) {
    return event_data.actions.map(function (v) {
        var vargs = JSON.stringify(v.args || []);
        return v.name + (v.type === 2 ? ('(' +
                vargs.slice(1, vargs.length-1) + ')') : '');
    }).join('.');
};

exports.run = function (opts, on_ready) {
    var child_process = require('child_process');
    var new_env = exports.extend({}, process.env, opts.env);
    var process_opts = { env: new_env };
    if (opts.cwd) { process_opts.cwd = opts.cwd}

    /* how to handle std out data */
    //if (opts.stdio) { process_opts.stdio = opts.stdio; }
    //else if (opts.passthrough) { process_opts.stdio = "inherit"; }
    //else { process_opts.stdio = "ignore";  }
    var child = child_process.spawn(opts.path, opts.args, process_opts);
    //child.unref();

    child.stdout.on('data', function (data) {
        var data_str = data.toString();
        if (opts.noisy) {
            console.log('stdout: ' + data_str);
        }

        if (data_str.indexOf(exports.SPECIAL_CODE) !== -1) {
            on_ready(child); // leave
        }
    });

    child.stderr.on('data', function (data) {
        var data_str = data.toString();
        if (opts.noisy) {
            console.log('stderr: ' + data_str);
        }

        if (data_str.indexOf(exports.SPECIAL_CODE) !== -1) {
            on_ready(child); // leave
        }
    });

    /*child.on('close', function (code) {
        if (opts.noisy) {
            console.log('child process exited with code ' + code);
        }
    });*/
};


