var lovelock = require('./index')

process.on('uncaughtException', function (err) {
    console.error("uncaught exception");
    console.error(err);
    console.error(err.stack);
    console.trace();
});

var NOOP = function () {};

var TEST_WINDOW = {
    math: {
        sum: function (a, b) {
            return a + b;
        },
        pi: 3.14
    },
    strings: {
        permute: function (a) {
            return {
                upper: a.toUpperCase(),
                lower: a.toLowerCase(),
            }
        }
    },
    time: {
        get_time: function () {
            return new Date().getTime();
        }
    }
};

var _clean_actions = function (actions) {
    actions.forEach(function (obj) {
        delete obj.type;
    });
};

exports.test_chaining = function (test) {
    test.expect(10);
    var _make_test_cb = function (expected_data_0, expected_data_1) {
        return function (data, callback) {
            _clean_actions(data.actions);
            test.equal(callback, NOOP);
            test.deepEqual(data.actions, expected_data_0);
            expected_data_0 = expected_data_1;
        };
    };

    // first test
    var test_1 = lovelock._new_magic_obj(_make_test_cb(
            [
                { name: 'one',   },
                { name: 'two',   },
                { name: 'three', args: [], },
                { name: 'four',  args: ['a', 'b'], },
            ],
            [ { name: 'another',   }, { name: 'test',   }, ]
            ));
    test_1.one.two.three().four('a', 'b')._get(NOOP);
    test_1.another.test._get(NOOP);

    // test _wait and _collect cmds
    var test_2 = lovelock._new_magic_obj(_make_test_cb([
                { name: 'one',   },
                { name: '_wait', timeout: 100 },
                { name: 'three', args: [], },
                { name: '_collect',  value_name: 'test', },
                { name: 'five',  },
                { name: '_end',  },
                { name: 'seven', },
            ]));
    test_2.one._wait(100).three()._collect('test').five._end().seven._get(NOOP);

    // test new / baked
    var test_3 = lovelock._new_magic_obj(_make_test_cb(
            [
                { name: 'one',   },
                { name: 'two',   },
                { name: 'three', args: [], },
            ],
            [
                { name: 'one',   },
                { name: 'two',   },
                { name: 'first', args: [], },
                { name: '_end',  },
                { name: 'one',   },
                { name: 'two',   },
                { name: 'second', args: [], },
                { name: '_end',  },
                { name: 'one',   },
                { name: 'two',   },
                { name: 'third', args: [], },
            ]
            ));
    var test_3_two = new test_3.one.two();
    test_3_two.three()._get(NOOP);

    // try with many ends
    test_3_two.first()._end()
    test_3_two.second()._end()
    test_3_two.third();
    test_3_two._get(NOOP);
    test.done();
};



exports.test_fake_roundtrip = function (test) {
    // Test round trip actions, but short-circuit the transport
    var fake_sender = function (data, callback) {
        lovelock._do_event(TEST_WINDOW, data, function (err, data) {
            callback(data); 
        });
    };

    var window = lovelock._new_magic_obj(fake_sender);

    _test_lovelock_roundtrip(test, window, function () {
        test.done();
    });
};

exports.test_local_roundtrip = function (test) {
    // Test full round trip operations

    // First create server, then continue test in the next
    // function
    lovelock.server(TEST_WINDOW, 'localhost', 9797, 'lolpw', function (server) {
        var window = lovelock.connect_remote({
            password: 'lolpw',
            port: 9797,
            host: 'localhost',
            fatal_value_error: true,
            remote: true,
        });

        _test_lovelock_roundtrip(test, window, function () {
            server.close();
            test.done();
        });
    });
};


var _test_lovelock_roundtrip = function (test, window, teardown, skip_expect) {
    var count = 6;
    if (!skip_expect) { test.expect(count); }

    var done = function () {
        count--; if (count === 0) { teardown(); } };

    window.math.pi._get(function (value) {
        test.equal(value, 3.14);
        done();
    });

    window.math.sum(5, 11)._get(function (value) {
        test.equal(value, 16);
        done();
    });

    window.strings.permute('TeSt').upper._get(function (value) {
        test.equal(value, 'TEST');
        done();
    });

    window.math.pi._collect('pi')._end().math.sum(1, 1)._collect('two')._get(function (results) {
        test.equal(results.pi, 3.14); done();
        test.equal(results.two, 2); done();
    });

    // make sure time out works
    window.time.get_time()._collect('t1')._end()._wait(50).time.get_time()._collect('t2')._get(function (results) {
        test.ok((results.t2 - results.t1) >= 50); done();
    });
};

var CHILD_PID = 999;
var FAKE_ELECTRON_PATH = "/fake/path/electron";

var _mock_child_process = function (test, server_callback) {
    return {
        exec: function (cmd, callback) {
            if (cmd === "which electron") {
                callback(null, FAKE_ELECTRON_PATH + "\n", "");
            } else {
                callback(new Error("testing unknown cmd"), "", "");
            }
        },

        spawn: function (path, args, opts) {
            if (path === FAKE_ELECTRON_PATH) {
                // pass
            } else {
                callback(new Error("testing unknown cmd"), "", "");
            }

            test.deepEqual(args, ['.', '--something']);

            var fake_io_on = function (signal, callback) {
                // start server
                lovelock.try_server(TEST_WINDOW, server_callback, { log: callback },
                                    opts.env);
            };

            // Mock child
            return {
                stdout: { on: NOOP, },
                stderr: { on: fake_io_on, },
                pid: CHILD_PID,
            }
        },
    };
};

exports.test_launch_electron = function (test) {
    var mockery = require('mockery');
    mockery.enable();
    mockery.warnOnUnregistered(false);
    test.expect(10);
    var server_teardown = null;

    mockery.registerMock('child_process', _mock_child_process(test, function (server) {
            test.ok(server, "server started");
            server_teardown = function () { server.close(); };
        }));

    mockery.registerMock('tree-kill', function (pid, signal, callback) {
        test.equal(signal, 'SIGTERM');
        test.equal(pid,    CHILD_PID);
        setTimeout(function () {
            check(0, callback);
        }, 5);
    });

    var check = function (count, cb) {
        if (count > 10) { throw new Error("tried too many times"); }
        else if (!server_teardown) {
            setTimeout(function () { check(count+1, cb) }, 100);
        } else {
            server_teardown();
            cb();
        }
    };

    lovelock.launch_electron({ args: ['--something'], }, function (window, teardown) {
        _test_lovelock_roundtrip(test, window, function () {
            teardown(function () {
                mockery.disable();
                test.done();
            });
        }, true);
    });
};

